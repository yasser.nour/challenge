package com.company.challenge.service;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.company.challenge.model.Vehicle;
import com.company.challenge.repositories.VehicleRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class VehicleServiceTest {

    @Resource
    private VehicleRepository vehicleRepository;
    
    @Test
    public void getAllVehicles() throws Exception {
    	List<Vehicle> vehilces= vehicleRepository.findAll();
    	assertNotNull(vehilces);
    }
}

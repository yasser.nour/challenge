angular.module( 'challengeApp', ['ngRoute'] ).config(function($routeProvider) {
	  $routeProvider
	  .when('/', { 
	      templateUrl : 'vehicles.html',
	      controller  : 'VehicleController'
	  });

	});

angular.module('challengeApp').service('VehicleService', function ($http, $q) {


    this.getVehicles = function () {
        var promise = $http({
            method: 'GET',
            url:  "api/vehicle/",
            headers: {'Content-Type': 'application/json;'}
        }).then(function (data) {
            return data
        }, function (error) {
            return $q.reject(error);
        });
        return promise;
    };
});
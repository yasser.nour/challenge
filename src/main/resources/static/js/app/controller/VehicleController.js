angular.module('challengeApp').controller('VehicleController', function ($scope, VehicleService) {

	$scope.success = null;
	$scope.error = null;

	$scope.attributes = [
        {key: "Vehicle ID", value: "vehicleId"},
        {key: "regesteration Number", value: "regNumber"},
        {key: "Customer Name", value: "customer"},
        {key: "Status", value: "status"}];
	
    var loadVehicles = function () {
    	VehicleService.getVehicles().then(function (data) {
            $scope.vehicles = data.data;

            $scope.search = '';
            $scope.sortAttribute = '';
        }, function (error) {
            if (error.data.message) $scope.error = error.data.message;
            else $scope.error = $scope.$parent.error;
        })
    };
    loadVehicles();
})
;

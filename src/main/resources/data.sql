insert into customer(id,name,address) values(1,'Kalles Grustransporter AB','Cementvägen 8, 111 11 Södertälje');
insert into customer(id,name,address) values(2,'Johans Bulk AB','Balkvägen 12, 222 22 Stockholm');
insert into customer(id,name,address) values(3,'Haralds Värdetransporter AB','Budgetvägen 1, 333 33 Uppsala');
insert into vehicle (id,vehicleId,regNumber,status,customer_id) values(1,'YS2R4X20005399401','ABC123','CONNECTED',1);
insert into vehicle (id,vehicleId,regNumber,status,customer_id) values(2,'VLUR4X20009093588','DEF456','CONNECTED',1);
insert into vehicle (id,vehicleId,regNumber,status,customer_id) values(3,'VLUR4X20009048066','GHI789','CONNECTED',1);
insert into vehicle (id,vehicleId,regNumber,status,customer_id) values(4,'YS2R4X20005388011','JKL012','CONNECTED',2);
insert into vehicle (id,vehicleId,regNumber,status,customer_id) values(5,'YS2R4X20005387949','MNO345','DISCONNECTED',2);
insert into vehicle (id,vehicleId,regNumber,status,customer_id) values(6,'VLUR4X20009048065','PQR678','CONNECTED',3);
insert into vehicle (id,vehicleId,regNumber,status,customer_id) values(7,'YS2R4X20005387055','STU901','DISCONNECTED',3);
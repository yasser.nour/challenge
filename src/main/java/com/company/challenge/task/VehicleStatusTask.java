package  com.company.challenge.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.company.challenge.common.VehicleStatus;
import com.company.challenge.model.Vehicle;
import com.company.challenge.service.VehicleService;

@Component
@Transactional
public class VehicleStatusTask {

    private static final Logger log = LoggerFactory.getLogger(VehicleStatusTask.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
	VehicleService vehicleService; 
    
    @Value("${status.service.url}")
	private String vehicleStatusServiceURL;
    
    RestTemplate restTemplate = getRestTemplate();
    
    @Scheduled(fixedRateString = "${status.service.rate}")
    public void updateVehicleStatus() {
    	ResponseEntity<String> response = null;
        log.info("The time is now {}", dateFormat.format(new Date()));
        List<Vehicle> vehicles = vehicleService.findAllVehicles();
        if (vehicles != null) {
        	log.info("Vehicles size is {}", vehicles.size());
        	for (Vehicle vehicle : vehicles) {
        		try {
        			response = restTemplate.getForEntity(vehicleStatusServiceURL + vehicle.getVehicleId(), String.class);
        			if (response.getStatusCode() == HttpStatus.OK) {
        				log.info("Vehicle {}", vehicle.getVehicleId() +" is connected");
            			if (!vehicle.getStatus().equals(VehicleStatus.CONNECTED)) {
            				vehicle.setStatus(VehicleStatus.CONNECTED);
                			vehicleService.updateVehicle(vehicle);
            			}
            		} else {
            			log.info("Vehicle {}", vehicle.getVehicleId() +" is disconnected");
            			if (!vehicle.getStatus().equals(VehicleStatus.DISCONNECTED)) {
            				vehicle.setStatus(VehicleStatus.DISCONNECTED);
                			vehicleService.updateVehicle(vehicle);
            			}
            		}
        		}
        		catch (HttpClientErrorException ex) {
        			log.info("Vehicle {}", vehicle.getVehicleId() +" is disconnected");
        			vehicle.setStatus(VehicleStatus.DISCONNECTED);
        			vehicleService.updateVehicle(vehicle);
        		}
        		log.info("Vehicle with vehicleId {}", vehicle.getVehicleId() + " updated");
			}
        }
    }
    
    private RestTemplate getRestTemplate() {
		RestTemplate restTemplate = null;
	        HttpClientBuilder clientBuilder = HttpClientBuilder.create();
	        HttpClient httpClient = clientBuilder.build();
	        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
	        factory.setHttpClient(httpClient);
			restTemplate = new RestTemplate(factory);
		return restTemplate;
	}
}
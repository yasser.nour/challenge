package com.company.challenge.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.challenge.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}

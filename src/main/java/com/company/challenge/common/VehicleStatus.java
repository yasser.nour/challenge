package com.company.challenge.common;

/**
 * Vehicle status values.
 * 
 */

public enum VehicleStatus {

	CONNECTED, DISCONNECTED;
}

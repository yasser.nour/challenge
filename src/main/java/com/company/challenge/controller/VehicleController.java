package com.company.challenge.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.company.challenge.model.Vehicle;
import com.company.challenge.service.VehicleService;

@RestController
@RequestMapping("/api")
public class VehicleController {

	public static final Logger logger = LoggerFactory.getLogger(VehicleController.class);

	@Autowired
	VehicleService vehicleService; 

	// -------------------Retrieve All Vehicles---------------------------------------------

	@RequestMapping(value = "/vehicle/", method = RequestMethod.GET)
	public ResponseEntity<List<Vehicle>> getAllVehicles() {
		logger.info("fetching All vehicles");
		List<Vehicle> vehicles = vehicleService.findAllVehicles();
		if (vehicles.isEmpty()) {
			logger.info("No Vehicles found");
			return new ResponseEntity<List<Vehicle>>(HttpStatus.NO_CONTENT);
		}
		logger.info("vehicles {}", vehicles.get(0).getCustomer().getName());
		return new ResponseEntity<List<Vehicle>>(vehicles, HttpStatus.OK);
	}
}
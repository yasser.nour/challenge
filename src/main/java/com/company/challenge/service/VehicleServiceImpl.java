package com.company.challenge.service;

import java.util.List;

import com.company.challenge.repositories.VehicleRepository;
import com.company.challenge.model.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service("vehicleService")
@Transactional
public class VehicleServiceImpl implements VehicleService{

	@Autowired
	private VehicleRepository vehicleRepository;

	
	public List<Vehicle> findAllVehicles(){
		return vehicleRepository.findAll();
	}
	
	public void saveVehicle(Vehicle vehicle) {
		vehicleRepository.save(vehicle);
	}
	
	public void updateVehicle(Vehicle vehicle){
		saveVehicle(vehicle);
	}
}

package com.company.challenge.service;


import java.util.List;

import com.company.challenge.model.Vehicle;

public interface VehicleService {
	
	void saveVehicle(Vehicle vehicle);

	void updateVehicle(Vehicle vehicle);
	
	List<Vehicle> findAllVehicles();

}